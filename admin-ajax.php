<?php

include '../../../wp-load.php';   

if( !current_user_can('manage_options') )
exit;

if( !isset($_POST['wth_action']) && $_POST['wth_action'] == '' )
exit;

$action = $_POST['wth_action'];

foreach( array('update_status_tag', 'update_status_rule','get_match_options') as $value )

add_action( 'wth_admin_ajax_'.$value, 'wth_'.$value );

do_action( 'wth_admin_ajax_'.$action );

function wth_update_status_tag(){
  
  global $wpdb;
  $wpdb->show_errors();
  $tag_id = $_POST['tag_id'];
  $status = $_POST['status'];
  
  if($status == '1')
  $status_ch = 0;	  
  else if($status == '0')
  $status_ch = 1;
  
  $wpdb->update($wpdb->prefix.'tags', array('status' => $status_ch), array('tag_id' => $tag_id));
  
  echo json_encode(array('status' => $status_ch));
  	
}

function wth_update_status_rule(){
  
  global $wpdb;
  
  $rule_id = $_POST['tag_id'];
  $status = $_POST['status'];
  
  if($status == '1')
  $status_ch = 0;	  
  else if($status == '0')
  $status_ch = 1;
  
  $wpdb->update($wpdb->prefix.'rules', array('status' => $status_ch), array('rule_id' => $rule_id));
  
  echo json_encode(array('status' => $status_ch));
  	
}

function wth_get_match_options(){
  
  global $wpdb;
  
  $rule_type = $_POST['rule_type'];
  $rule_value = $_POST['rule_value'];
  
  $options.="<option value='all'>All</option>";
  if($rule_type=='page')
  {
	
	$all_pages=get_pages();
	
	foreach($all_pages as $page)
	 $options.="<option value='$page->ID' ".selected($page->ID,$rule_value,false).">$page->post_title</option>";
  }
  elseif($rule_type=='post')
  {
	  $all_posts=get_posts();
	
		

		foreach($all_posts as $post)
	 $options.="<option value='$post->ID' ".selected($post->ID,$rule_value,false).">$post->post_title</option>";
	  
	}
	elseif($rule_type=='post_category')
  {
	  $all_categorys=get_categories();
	
	foreach($all_categorys as $category)
	 $options.="<option value='$category->term_id' ".selected($category->term_id,$rule_value,false).">$category->name</option>";
	  
	}
	elseif($rule_type=='post_type')
  {
	
	$args = array(
   'public'   => true,
   '_builtin' => false
);

$output = 'names'; // names or objects, note names is the default
$operator = 'and'; // 'and' or 'or'

$all_post_type = get_post_types( $args, $output, $operator ); 
	
	foreach($all_post_type as $slug=>$name)
	 $options.="<option value='".$slug."' ".selected($slug,$rule_value,false).">$name</option>";
	  
	} 
   elseif($rule_type=='widget')
  {
	$all_sidebars=get_option('sidebars_widgets');
	
	foreach($all_sidebars as $name=>$widgets)
	 {
		 if(is_array($widgets))
		 {
		 foreach($widgets as $key=>$widget)
			$options.="<option value='$widget' ".selected($widget,$rule_value,false).">$widget</option>";
		}
	  }
	}
	 
   
  
  if($rule_type!='regx')
  echo "<select name='rule_to_value' id='rule_to_value'>$options</select>";
  else
  echo "<input name='rule_to_value' id='rule_to_value' value='".stripslashes($rule_value)."'>";
  	
}


exit;





