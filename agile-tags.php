<?php
/*
  Plugin Name: Agile Tags

  Description: An excellent way for A/B Testings.

  Version: 1.0.0

  Author: Sandeep Kumar

  Author URI: www.funnelenvy.com

 */

add_action('init', 'wtm_manage_init');

include('configuration.php');

include( CLASSES_PATH . '/class.tags.public.php');
new Tag_Fronted();

/**
 * This function used to register all hooks.
 */
function wtm_manage_init(){
	wp_enqueue_style( 'wtm_manage_plugin_style1', plugins_url( '/css/bootstrap/css/bootstrap.css', __FILE__ ) );
	wp_enqueue_style( 'wtm_manage_plugin_style3', plugins_url( '/css/wp-tag-management.css', __FILE__ ) );
	wp_enqueue_script('jquery');
	wp_enqueue_script('wtm_manage_plugin_scrpt', plugins_url( '/css/bootstrap/js/bootstrap.min.js', __FILE__ ));
	add_action('admin_menu', 'register_wtm_manage_menu_page');
	

}

if( is_admin() )
add_action( 'admin_enqueue_scripts', 'wtm_admin_enqueue_scripts' );

function wtm_admin_enqueue_scripts(){
	
	wp_enqueue_script( 'general', plugins_url('js/admin-script.js',__FILE__), array('jquery') );
	wp_localize_script( 'general', 'wtm', array('ajaxUrl' => plugins_url('admin-ajax.php', __FILE__) ) );
}

/**
 * This function used to display Wp Tag Management menu in backend.
 */
function register_wtm_manage_menu_page(){
	
	add_menu_page('Agile Tags', 'Agile Tags', 'manage_options','wtm_manage', 'wtm_manage_display', '');
	
	//add_submenu_page('wtm_manage', 'Add New Tag','Add New Tag','manage_options','wtm_add_tag', 'wtm_add_tag');
	
	//add_submenu_page('wtm_manage', 'Add New Rule','Add New Rule','manage_options','wtm_add_rule', 'wtm_add_rule');
	
	

}

/**
 * This function used to display WP Tag Management system.
 */


function wtm_manage_display(){
	
	$tabs = array( 'wtm_add_tag' => 'Add New Tag','manage_tags' => 'Manage Tags','wtm_add_rule' => 'Add New Rule','manage_rules' => 'Manage Rules','settings' => 'Settings' );
	
	echo '<div class="wrap">';
	
	echo '<h2 class="nav-tab-wrapper">';
	
	$current_tab = $_GET['tab'] ? $_GET['tab'] : 'wtm_add_tag'; 
	
	foreach($tabs as $tab => $tab_title ){
      
      $active_class = '';
      
      if( $current_tab == $tab )
      
      $active_class = 'nav-tab-active';
      
     echo '<a href="'.admin_url('admin.php?page=wtm_manage&tab='.$tab).'" class="nav-tab '.$active_class.'">'.$tab_title.'</a>'; 		 
	
	}
	
	echo '</h2>';
	
	echo '<br>';
	
	switch( $current_tab ){
		
	  case 'wtm_add_tag'	:   wtm_add_tag();    break;	
	 
	  case 'manage_tags'	:   wtm_manage_tags();    break;
	  
	  case 'wtm_add_rule'	:   wtm_add_rule();    break;
	  
	  case 'manage_rules'	:   wtm_manage_rules();  break;
	  
	  default : wtm_manage_settings();
		
	}
	
	echo '</div>';
	
}

/**
 * This function used to display WP Tag Management in backend.
 */



/**
 * This function used to add tag in backend.
 */
function wtm_add_tag(){
	
	include( CLASSES_PATH . '/class.tags.php');
	
	$tag = new Tag();
	
	echo $tag->add_form();
	
}

/**
 * This function used to manage tags in backend.
 */
function wtm_manage_tags(){
	
	include( CLASSES_PATH . '/class.tags.php');
	
	include( CLASSES_PATH . '/class.rules.php');
	
	$tag = new Tag();
	
	$action = $_GET['action'] ? $_GET['action'] : '';
	
	$tag->load($_GET['tag_id']);
	
	switch($action){
	  
	  case 'edit' :  echo $tag->edit_form();  break;
	  
	  case 'add' :   echo $tag->add_form();   break;
	  
	  case 'delete' : $tag->delete();  echo $tag->manage_form(); break;
	  
	    default  :   echo $tag->manage_form();
	
		
	} 
	
	
	
}


/**
 * This function used to add rule in backend.
 */
function wtm_add_rule(){
	
	include( CLASSES_PATH . '/class.rules.php');
	
	$rule = new Rule();
	
	
	
	echo $rule->add_form();

}



/**
 * This function used to manage rules in backend.
 */
function wtm_manage_rules(){
	
	include( CLASSES_PATH . '/class.rules.php');
	
	$rule = new Rule();
	
	$action = $_GET['action'] ? $_GET['action'] : '';
	
	$rule->load($_GET['rule_id']);
	
	switch($action){
	  
	  case 'edit' :  echo $rule->edit_form();  break;
	  
	  case 'add' :  echo $rule->add_form();  break;
	  
	  case 'delete' : $rule->delete();  echo $rule->manage_form(); break;
	  
	    default  :   echo $rule->manage_form();
	
		
	} 
	
	
	
}

function wtm_manage_settings()
{
	$wtm_settings=get_option('wtm_settings');
?>
<div class="wrap">  
<div id="icon-options-general" class="icon32"><br></div>
		<h2><?php _e( 'Settings', 'wtm_language' ) ?></h2><br>
        <form class='form' method="post" action="options.php">  
        <?php wp_nonce_field('update-options') ?>
        <div class='form-groups'> 
		<input type='checkbox' name='wtm_settings' id='wtm_settings' <?php checked($wtm_settings,'true'); ?> value='true' >&nbsp;&nbsp;Enable Console Logging for Admins
		</div> 
		<input type="hidden" name="action" value="update" />  
		<input type="hidden" name="page_options" value="wtm_settings" />  
		<p class="submit">
			<input type="submit" name="submit" id="submit" class="button button-primary" value="<?php _e( 'Save', 'wtm_language' ) ?>">
		</p>
		</form>
 </div>
     	
<?php	
}
/**
 * This function used to show success/failure message in backend.
 */
function wtm_manage_show_message($message, $errormsg = false){
	if(empty($message))
		return;
	if ($errormsg)
		echo '<div id="message" class="error">';
	else 
		echo '<div id="message" class="updated fade">';
	echo "<p><strong>$message</strong></p></div>";
}


register_activation_hook(__FILE__, 'wtm_manage_plugin_activation');

register_deactivation_hook(__FILE__, 'wtm_manage_plugin_deactivation'); 

/**
 * This function used to install required tables in database.
 */
function wtm_manage_plugin_activation(){
	
	global $wpdb;
	
	$tags_table="CREATE TABLE IF NOT EXISTS `".TBL_TAGS."` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `content` text NOT NULL,
  `rules` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`tag_id`)
) ENGINE=MyISAM";

	$wpdb->query($tags_table);
	
	$rule_table="CREATE TABLE IF NOT EXISTS `".TBL_RULES."` (
  `rule_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `match_me` varchar(255) NOT NULL,
  `to_value` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rule_id`)
) ENGINE=MyISAM;";

	$wpdb->query($rule_table);
	
	$tag_rules="CREATE TABLE IF NOT EXISTS `".TBL_TAG_RULES."` (
  `rel_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) NOT NULL,
  `rule_id` int(11) NOT NULL,
  PRIMARY KEY (`rel_id`),
  UNIQUE KEY `rel_id` (`rel_id`)
) ENGINE=InnoDB;";

	$wpdb->query($tag_rules);

}

/**
 * This function used to do required action on deactivation.
 */
function wtm_manage_plugin_deactivation(){
}

function wtm_set_action_url($query_args){
  	

 
 return add_query_arg($query_args);

}


