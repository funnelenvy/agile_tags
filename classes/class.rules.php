<?php
/*
	This SQL query will create the table to store your object.

	CREATE TABLE IF NOT EXISTS `pw_rules` (
  `rule_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `match_me` varchar(255) NOT NULL,
  `to_value` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rule_id`)
) ENGINE=MyISAM;

*/

/**
* <b>rule</b> class with integrated CRUD methods.
* @author Sandeep Kumar
* @version 1.0
*/
include_once( 'class.base.php' );

class Rule extends Plugin_Base
{
	protected $rule_id = '';

	/**
	 * @var VARCHAR(255)
	 */
	public $name;
	
	/**
	 * @var INT
	 */
	public $type;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $match_me;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $to_value;
	
	
	protected $valiations=array(
	
	'name'=>array('req'=>'Rule name is required.'),
	'type'=>array('req'=>'Rule type is required.'),
	'match_me'=>array('req'=>'Configuration is required.'),
	'to_value'=>array('req'=>'Equals value is required.'),
	
	);
	
	function Rule( $name='', $type='', $match_me='', $to_value='', $status = '' )
	{
		global $configuration;
		$this->name = $name;
		$this->type = $type;
		$this->match_me = $match_me;
		$this->to_value = $to_value;
		$this->status = $status;
		$this->table=$configuration['prefix']."rules";
		$this->unique='rule_id';
		
		$this->action();
	}
	
	function action()
	{
		
		if( $_POST['submit_add_rule'] )
		{
			$this->setVal('name',$_POST['rule_name']);
			$this->setVal('type',$_POST['rule_type']);
			$this->setVal('match_me',$_POST['rule_match_me']);
			$this->setVal('to_value',$_POST['rule_to_value']);
			
			if( empty($this->errors) )
			{
					$insert_id = $this->save();
					
					if( $insert_id >0 )
					{
						$this->success = "Rule added successfully.";
					}
					else
					{
						$this->errors['wrong'] = 'Something went wrong. Try again!';
					}
			}
		}
	
	
		 if( $_POST['submit_edit_rule'] )
		{
			$this->setVal('rule_id',$_GET['rule_id']);
			$this->setVal('name',$_POST['rule_name']);
			$this->setVal('type',$_POST['rule_type']);
			$this->setVal('match_me',$_POST['rule_match_me']);
			$this->setVal('to_value',$_POST['rule_to_value']);
			
			if( empty($this->errors) )
			{
					$insert_id = $this->save();
					
					if( $insert_id >0 )
					{
						$this->success = "Rule updated successfully.";
					}
					else
					{
						$this->errors['wrong'] = 'Something went wrong. Try again!';
					}
			}
		}
				

		
	}

	/**
	* Load the object from the database
	* @return Void
	*/
	function load($load_id)
	{ 
		$object=$this->Get(array(array("rule_id","=",$load_id)));
		
		if(isset($object))
		{
				$this->fill($object[0]);	
		}
	}
	
	
	function fill($row)
	{
			$this->setVal('rule_id',$row->rule_id);
			$this->setVal('name',$row->name);
			$this->setVal('type',$row->type);
			$this->setVal('match_me',$row->match_me);
			$this->setVal('to_value',$row->to_value);
			$this->setVal('status', $row->status);
			$this->tags=$this->get_tags();		
	}
	
	
	/**
	* Returns a sorted array of objects that match given conditions
	* @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...} 
	* @param string $sortBy 
	* @param boolean $ascending 
	* @param int limit 
	* @return array $ruleList
	*/
	function Get($fcv_array = array(), $sortBy='', $ascending=true, $limit='')
	{
		$connection = Database::Connect();
		$sqlLimit = ($limit != '' ? "LIMIT $limit" : '');
		$this->query = "SELECT * FROM $this->table ";
		$ruleList = Array();
		if (sizeof($fcv_array) > 0)
		{
			$this->query .= " WHERE ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$this->query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) != 1)
					{
						$this->query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						if ($GLOBALS['configuration']['db_encoding'] == 1)
						{
							$value = Plugin_Base::IsColumn($fcv_array[$i][2]) ? "BASE64_DECODE(".$fcv_array[$i][2].")" : "'".$fcv_array[$i][2]."'";
							$this->query .= "BASE64_DECODE(`".$fcv_array[$i][0]."`) ".$fcv_array[$i][1]." ".$value;
						}
						else
						{
							$value =  Plugin_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$this->Escape($fcv_array[$i][2])."'";
							$this->query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
						}
					}
					else
					{
						$value = Plugin_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$fcv_array[$i][2]."'";
						$this->query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
					}
				}
			}
		}
		if ($sortBy != '')
		{
			if (isset($this->pog_attribute_type[$sortBy]['db_attributes']) && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'SET')
			{
				if ($GLOBALS['configuration']['db_encoding'] == 1)
				{
					$sortBy = "BASE64_DECODE($sortBy) ";
				}
				else
				{
					$sortBy = "$sortBy ";
				}
			}
			else
			{
				$sortBy = "$sortBy ";
			}
		}
		else
		{
			$sortBy = $this->unique;
		}
		$this->query .= " ORDER BY ".$sortBy." ".($ascending ? "ASC" : "DESC")." $sqlLimit";
		$thisObjectName = get_class($this);
		$cursors = Database::Reader($this->query, $connection);
		
		foreach( $cursors as $row)
		{
			
			$rule = new $thisObjectName();
			$rule->fill($row);
			$objects[] = $rule;
		}
		
		return $objects;
	}
	
	

	
	
	/**
	* Saves the object to the database
	* @return integer $rule_id
	*/
	function Save()
	{
		$connection = Database::Connect();
		$rows = 0;
		if ( $this->rule_id != '' ){
			$this->query = $connection->prepare("SELECT $this->unique FROM $this->table WHERE $this->unique='%d' LIMIT 1",$this->rule_id);
			$rows = Database::Query($this->query, $connection);
		}
		
			$data['name'] = $this->Escape($this->name);
			$data['type']  = $this->Escape($this->type);
			$data['match_me'] = $this->Escape($this->match_me);
			$data['to_value'] = stripslashes($this->to_value);
		
		if ($rows > 0 )
		{
		$where['rule_id']=$this->Escape($this->rule_id);
		}
		else
		{
			$where='';
		}
	
		$insertId = Database::InsertOrUpdate($this->table,$data,$where);
		
		if ($this->rule_id == "")
		{
			$this->rule_id = $insertId;
		}
		return $this->rule_id;
	}
	
	
	/**
	* Deletes the object from the database
	* @return boolean
	*/
	function Delete()
	{
		$connection = Database::Connect();
		$this->query = $connection->prepare("DELETE FROM $this->table WHERE $this->unique='%d'",$this->rule_id);
		return Database::NonQuery($this->query, $connection);
	}
	
	/**
	 * View for Get Assigned Rules.
	 * @return HTML Structure to add Tag 
	 */
	
	function get_tags()
	{
		$connection = Database::Connect();
		$rows = 0;
		$this->query = $connection->prepare("SELECT * FROM ".TBL_TAG_RULES." as tr inner join ".TBL_TAGS." as t on tr.tag_id=t.tag_id WHERE tr.rule_id='%d'",$this->rule_id);
		$rows = Database::Reader($this->query, $connection);
		return $rows;
	}
	
	
	/**
	 * View for Add Rule
	 * @return HTML Structure to add Rule 
	 */
	 
	 public function add_form()
	 {
		 ob_start();
		 include( VIEWS_PATH. '/view-add-rule.php');
		 $output=ob_get_contents();
		 ob_clean();
		 
		 return $output;
	 }
	 

	 
	 public function edit_form()
	 {
		 ob_start();
		 include( VIEWS_PATH . '/view-edit-rule.php');
		 $output=ob_get_contents();
		 ob_clean();
		 
		 return $output;
	 }
	 
	 /**
	 * View for Manage Tags
	 * @return HTML Structure to manage Tags 
	 */
	 
	 public function manage_form()
	 {
		 $all_rules=$this->Get();
		 ob_start();
		 include( VIEWS_PATH . '/view-manage-rules.php');
		 $output=ob_get_contents();
		 ob_clean();
		 
		 return $output;
	 }
	 
	 
	
}
?>
