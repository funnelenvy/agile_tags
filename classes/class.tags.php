<?php
/*
	SQL Table required for this object.

	CREATE TABLE IF NOT EXISTS `prefix_tags` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `content` text NOT NULL,
  `rules` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`tag_id`)
) ENGINE=MyISAM  DEFAULT;
* 
*/

/**
* <b>tags</b> class with integrated CRUD methods.
* @author Sandeep Kumar
* @version 1.0.0
*/
include_once( 'class.base.php' );
class Tag extends Plugin_Base
{
	public $tag_id = '';

	/**
	 * @var VARCHAR(255)
	 */
	public $name;
	
	/**
	 * @var INT
	 */
	public $type;
	
	/**
	 * @var TEXT
	 */
	public $content;
	
	/**
	 * @var BOOLEAN
	 */
	public $status;
	
	public $rules;
	
	
	protected $valiations=array(
	
	'tag_id'=>array('req'=>'Tag ID is required.','int'=>'It should be numeric.'),
	'name'=>array('req'=>'This field is required.'),
	'type'=>array('req'=>'This field is required.'),
	'content'=>array('req'=>'Please insert Tags content.')
	);
	
	public $tag_types=array('1'=>'Custom HTML');
	
	function Tag( $name='', $type='', $content='', $rules='' )
	{
		global $configuration;
		$this->name = $name;
		$this->type = $type;
		$this->content = $content;
		$this->table=$configuration['prefix']."tags";
		$this->unique='tag_id';
		
		$this->action();
		
	}
	
	

	function action()
	{
		
		if( $_POST['submit_add_tag'] )
		{
			$this->setVal('name',$_POST['tag_name']);
			$this->setVal('type',$_POST['tag_type']);
			$this->setVal('content',$_POST['tag_content']);
			
			if( empty($this->errors) )
			{
					$insert_id = $this->save();
					
					if( $insert_id >0 )
					{
						$this->success = "Tag added successfully.";	
					}
					else
					{
						$this->errors['wrong'] = 'Something went wrong. Try again!';
					}
			}
		}
		
		if( isset($_POST['submit_edit_tag']) && !empty($_POST) )
		{
			$this->setVal('tag_id',$_GET['tag_id']);
			$this->setVal('name',$_POST['tag_name']);
			$this->setVal('type',$_POST['tag_type']);
			$this->setVal('content',$_POST['tag_content']);
			
			if( empty($this->errors) )
			{
					$insert_id = $this->save();
					
					if( $insert_id >0 )
					{
						$this->success = "Tag update successfully.";	
					}
					else
					{
						$this->errors['wrong'] = 'Something went wrong. Try again!';
					}
			}
		}
		
		if( isset($_POST['submit_assign_rule']) && !empty($_POST) )
		{
					
					$insert_id = $this->add_rule($_POST['rule'],$_GET['tag_id']);
					
					if( $insert_id >0 )
					{
						$this->success = "Rule added successfully.";	
					}
					
		}


	if( isset($_POST['submit_unassign_rule']) && !empty($_POST) )
		{
				
					$deleted = $this->delete_rule($_GET['tag_id'],$_POST['rule_id']);
					
					if( $deleted )
					{
						$this->success = "Rule unassigned successfully.";	
					}
					
		}

		
	}

	/**
	* Load the object from the database
	* @return Void
	*/
	function load($load_id)
	{ 
		$object=$this->Get(array(array("tag_id","=",$load_id)));
		
		if(isset($object))
		{
				$this->fill($object[0]);	
		}
	}
	
	/**
	* Fill the object from the database
	* @return Void
	*/
	function fill($row)
	{
			$this->setVal('tag_id',$row->tag_id);
			$this->setVal('name', $this->Unescape($row->name));
			$this->setVal('type',$this->Unescape($row->type));
			$this->setVal('content',stripcslashes($row->content));
			$this->setVal('status',$this->Unescape($row->status));
			
			$this->rules=$this->get_rules();
	}
		
	/**
	* Returns a sorted array of objects that match given conditions
	* @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...} 
	* @param string $sortBy 
	* @param boolean $ascending 
	* @param int limit 
	* @return array $tagsList
	*/
	function Get($fcv_array = array(), $sortBy='', $ascending=true, $limit='')
	{
		$connection = Database::Connect();
		$sqlLimit = ($limit != '' ? "LIMIT $limit" : '');
		$this->query = "SELECT * FROM $this->table ";
		$tagsList = Array();
		if (sizeof($fcv_array) > 0)
		{
			$this->query .= " WHERE ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$this->query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) != 1)
					{
						$this->query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						if ($GLOBALS['configuration']['db_encoding'] == 1)
						{
							$value = Plugin_Base::IsColumn($fcv_array[$i][2]) ? "BASE64_DECODE(".$fcv_array[$i][2].")" : "'".$fcv_array[$i][2]."'";
							$this->query .= "BASE64_DECODE(`".$fcv_array[$i][0]."`) ".$fcv_array[$i][1]." ".$value;
						}
						else
						{
							$value =  Plugin_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$this->Escape($fcv_array[$i][2])."'";
							$this->query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
						}
					}
					else
					{
						$value = Plugin_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$fcv_array[$i][2]."'";
						$this->query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
					}
				}
			}
		}
		if ($sortBy != '')
		{
			if (isset($this->pog_attribute_type[$sortBy]['db_attributes']) && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'SET')
			{
				if ($GLOBALS['configuration']['db_encoding'] == 1)
				{
					$sortBy = "BASE64_DECODE($sortBy) ";
				}
				else
				{
					$sortBy = "$sortBy ";
				}
			}
			else
			{
				$sortBy = "$sortBy ";
			}
		}
		else
		{
			$sortBy = $this->unique;
		}
		$this->query .= " ORDER BY ".$sortBy." ".($ascending ? "ASC" : "DESC")." $sqlLimit";
		
		
		$thisObjectName = get_class($this);
		
		
		
		$cursors = Database::Reader($this->query, $connection);
		if($cursors)
		{
			
			foreach ($cursors as $row)
			{
				
				$tags = new $thisObjectName();
				$tags->fill($row);
				$objects[] = $tags;

			}
		  return $objects;
	   }	  
	}
	
	

	
	/**
	* Saves the object to the database
	* @return integer $tag_id
	*/
	function Save()
	{
		$connection = Database::Connect();
		$rows = 0;
		if ( $this->tag_id != '' ){
			$this->query = $connection->prepare("SELECT $this->unique FROM $this->table WHERE $this->unique='%d' LIMIT 1",$this->tag_id);
			$rows = Database::Query($this->query, $connection);
		}
			$data['name']=$this->Escape($this->name);
			$data['type']=$this->Escape($this->type);
			$data['content']=$this->Escape($this->content);
			$data['status']=$this->Escape($this->status);
		
		if ($rows > 0 )
		{
		$where['tag_id']=$this->Escape($this->tag_id);
		}
		else
		{
			$where='';
		}
		
		$insertId = Database::InsertOrUpdate($this->table,$data,$where);
		
		if ($this->tag_id == "")
		{
			$this->tag_id = $insertId;
		}
		return $this->tag_id;
	}
	
	
	/**
	* Deletes the object from the database
	* @return boolean
	*/
	function Delete()
	{
		$connection = Database::Connect();
		$this->query = $connection->prepare("DELETE FROM $this->table WHERE $this->unique='%d'",$this->tag_id);
		return Database::NonQuery($this->query, $connection);
	}
	
	/**
	* Assign a rule to tag
	* @return boolean
	*/
	function add_rule($rule_id,$tag_id)
	{
		$connection = Database::Connect();
		$rows = 0;
		$this->query = $connection->prepare("SELECT $this->unique FROM ".TBL_TAG_RULES." WHERE tag_id='%d' and rule_id='%d' LIMIT 1",$tag_id,$rule_id);
		$rows = Database::Query($this->query, $connection);
		
		if ($rows > 0 )
		{
			$this->errors['duplicate']="Rule is already assigned.";
		}
		else
		{
			$data['tag_id']=$this->Escape($tag_id);
			$data['rule_id']=$this->Escape($rule_id);
		
			$insertId = Database::InsertOrUpdate(TBL_TAG_RULES,$data,$where);
			
			if ($insertId>0)
			{
				return true;
			}
			else
			{
				$this->errors['wrong']="Something went wrong. Try again!";
			}
		}
		
		return false;
	}
	
	/**
	* Unassigned the rule on the object from the database
	* @return boolean
	*/
	function delete_rule($tag_id,$rule_id)
	{
		$connection = Database::Connect();
		$this->query = $connection->prepare("DELETE FROM ".TBL_TAG_RULES." WHERE tag_id=%d and rule_id=%d",$tag_id,$rule_id);
		return Database::NonQuery($this->query, $connection);
	}
	
	/**
	 * View for Get Assigned Rules.
	 * @return HTML Structure to add Tag 
	 */
	
	function get_rules()
	{
	
		$connection = Database::Connect();
		$rows = 0;
		
		$this->query = $connection->prepare("SELECT * FROM ".TBL_TAG_RULES." as tr inner join ".TBL_RULES." as t on tr.rule_id=t.rule_id WHERE tr.tag_id='%d'",$this->tag_id);
		$rows = Database::Reader($this->query, $connection);
	
		return $rows;
	}

	/**
	 * View for Add Tags
	 * @return HTML Structure to add Tag 
	 */
	 
	 public function add_form()
	 {
		 ob_start();
		 include( VIEWS_PATH . '/view-add-tag.php');
		 $output=ob_get_contents();
		 ob_clean();
		 
		 return $output;
	 }
	 
	 
	 public function edit_form()
	 {
		 global $wpdb;
		 
			print_r($all_rules);
			ob_start();
		 
		 include( VIEWS_PATH . '/view-edit-tag.php');
		 
		 $output=ob_get_contents();
		 
		 ob_clean();
		 
		 return $output;
	 }
	 
	 /**
	 * View for Manage Tags
	 * @return HTML Structure to manage Tags 
	 */
	 
	 public function manage_form()
	 {
		 $all_tags=$this->Get();
		
		 ob_start();
		 include( VIEWS_PATH . '/view-manage-tags.php');
		 $output=ob_get_contents();
		 ob_clean();
		 
		 return $output;
	 }
}
?>
