<?php 

include_once( 'class.base.php' );

class Tag_Fronted extends Plugin_Base{
	
  var $rule_types = array();
  
  var $dynamic_widgets_ids = array();
  
  function __construct(){ 
	  
	  
	  
	   add_action( 'wp_footer', array( $this, 'tag_content_show' ) , 100);
	   
	   
	   if( ! is_admin() )
	  
	   add_action( 'dynamic_sidebar', array( $this, 'set_dynamic_widget_ids' ) );
	   
	  
  }
  
  function set_dynamic_widget_ids($widgets){
	
	 
	 $this->dynamic_widgets_ids[] = "'".$widgets['id']."'";  
  
  }
  
 
  function get_rules_ids(){
	  
		
	if( ! $this->rule_types && ! is_array($this->rule_types) )
	
	return;
	
	$connection = Database::Connect();
	
	
	foreach( $this->rule_types as $type => $to_value ){
						
		$query_string = "SELECT r.rule_id, tr.tag_id FROM ".TBL_RULES." r LEFT JOIN ".TBL_TAG_RULES." tr ON tr.rule_id = r.rule_id WHERE status = %d AND type = '%s' AND to_value IN ($to_value, '%s') AND match_me = %d ";
		
		$this->query = $connection->prepare($query_string, 1, $type, 'all', 1);
		
		$result =  Database::Reader($this->query, $connection);
		
		
		if( $result ){
		
		 foreach($result as $rule)	
		  
		  $this->rule_matches_tags[$rule->tag_id] = 	$rule->rule_id;
		
		}
				
		$this->query = $connection->prepare($query_string, 1, $type, 'all', 2);
		
		$result =  Database::Reader($this->query, $connection);
		
		
		
		if( $result ){
		  
		  foreach($result as $not_rule ){
			 			  
			  if( $this->rule_matches_tags && isset($this->rule_matches_tags[$not_rule->tag_id])  )
			  
			  unset($this->rule_matches_tags[$not_rule->tag_id]);
			  
		  }	
		
		}
		
		
	} 
	
	
  }
  
  function get_rules_tags(){
	
	if( !isset($this->rule_matches_tags) ) 
	return; 
	
	$tags = implode(",", array_keys($this->rule_matches_tags) );
	
	$connection = Database::Connect();
	
	$this->query = "SELECT * FROM ".TBL_TAGS." t WHERE t.status= 1 AND tag_id IN ($tags) ";
	
	
		
	$tags =  Database::Reader($this->query, $connection);
	
	
	
		
	foreach( $tags as $tag )
	{
	  echo $tag->content;	
	  
	}
	  
  }
  
  
  
  function tag_content_show(){
	
	global $wp_query, $wpdb;
	
 	$object_id = $wp_query->get_queried_object_id();
	 
	 if( is_single() ||  is_singular() ){
	     
	   $post_type  =  get_post_type() ? get_post_type() : 'post';
	   
	   $this->rule_types[$post_type] = "'".$object_id."'";
	     
	 }
	 else if( is_category() ){
		 
		 $this->rule_types['post_category'] = "'".$object_id."'";
		
	 }else if( is_post_type_archive() ) {
		 
		$post_type =  get_post_type() ? get_post_type() : '';
		
		if( $post_type ){
		
		  $this->rule_types[$post_type] = 'all';
		
		}
		 
	 }
	 
	 $current_url = 'http://';
	
	 $current_url .=  ($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME'];
	
	 $current_url .= $_SERVER['REQUEST_URI'];
	 
	 $match_path = parse_url($current_url, PHP_URL_PATH );
	 
	 $home_path = parse_url( home_url(), PHP_URL_PATH );

	 if($home_path){
	
	  $home_path = rtrim($home_path, '/' );	 
	
	  $match_path = str_replace($home_path.'/', " ",$match_path);
		 
	 }
	 
	 $match_path = ltrim( $match_path, '/' );
	 
	 if( $match_path ){
		 
		 $regex_rules = $wpdb->get_col("select to_value from ".TBL_RULES." where type='regx' and status = 1 ");
		 
		 if($regex_rules){
			
			 foreach($regex_rules as $r_rule ){
			  
			  $tm = $r_rule;

			  $r_rule = trim($r_rule);
			  
			  if($r_rule[0] == "/" )
			 
			  $r_rule = substr($r_rule, 1 );
			 
			 
			  if( @preg_match( "!^$r_rule!", trim($match_path) ) )
			  {
				  $this->rule_types['regx'] = "'".$tm."'";
			  }
		 
		   }
			 
		 }
	 }
	    
	 if( $this->dynamic_widgets_ids && is_array( $this->dynamic_widgets_ids ) )
	 
	 $this->rule_types['widget'] = implode(", ", $this->dynamic_widgets_ids);

	 $this->get_rules_ids();
	 
	 $this->get_rules_tags();
  
  
  }
	
}

