<?php
//IMPORTANT:
//Rename this file to configuration.php after having inserted all the correct db information

global $configuration,$wpdb;
$configuration['prefix'] = $wpdb->prefix;

define('CLASSES_PATH',plugin_dir_path( __FILE__ ).'/classes');
define('VIEWS_PATH',plugin_dir_path( __FILE__ ).'views');

define("TBL_TAG_RULES",     $configuration['prefix']."tag_rules");
define("TBL_RULES",     $configuration['prefix']."rules");
define("TBL_TAGS",     $configuration['prefix']."tags");


?>
