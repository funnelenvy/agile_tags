jQuery(document).ready(function($){
   
   ajax_args = {
	 
	 url :   wtm.ajaxUrl,
	 
	 type:  'POST',
	 
	 data: { wth_action: '' },
	 
	 dataType : ''  
	   
   }
   
   
   $("a.tag-status, a.rule-status").click(function(){
	
	 tag_id  = $(this).attr('data-id');
	 
	 tag_status = $(this).attr('data-status');
	 
	 elem = $(this); 

	 ajax_args.data.wth_action = 'update_status_tag';
	 
	 if( elem.hasClass('rule-status') )
	 ajax_args.data.wth_action = 'update_status_rule';
	 
	 ajax_args.data.tag_id = tag_id;
	 
	 ajax_args.data.status = tag_status;
	 
	 ajax_args.dataType ='json'
	 
	 $.ajax(ajax_args).done(function(data){
         
         status_on_off = data.status ? 1 : 0;
            
         if( data.status == 1){
	   
          btn_status = 'Pause';
          
          status = 'Active';   	 
	 
         }else{
	   
           btn_status = 'Active';
           
           status = 'Pause';	 	 
	 }    
		 elem.attr('data-status', status_on_off);
		 elem.text(btn_status);
		 elem.parent().prev().text(status);
	 });	
   
   
   })


$("#rule_type").change(function(){
	
	 rule_type  = $(this).val();
	 
	 elem = $(this); 
	 
	 ajax_args.data.wth_action = 'get_match_options';
	 
	 ajax_args.data.rule_type = rule_type;
	
	ajax_args.data.rule_value=$('#selected_to_value').val();
	
	ajax_args.dataType ='html';

	 $.ajax(ajax_args).done(function(data){
    
     $('#rule_values_container').html(data);
            
     });	
   
   
   })
 $("#rule_type").trigger('change');
	
})

   
function unassign_rule(rule_id)
{
jQuery('#rule_id').val(rule_id);
jQuery('#unassign_rules').submit();
}
