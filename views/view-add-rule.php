<div class='wrap'>
	<div class="container-fluid">	

	<h2>Add a Rule</h2>
					
			<div class='row'>
			
			<div class='col-sm-7'>

<?php			
			if( $this->errors != '' ) :
			echo "<p class='bg-danger'>
			
			".$this->display_errors()."
			
			</p>";
			endif;
			
			if( $this->success != '' ) :
			echo "<p class='bg-success'>
			
			".$this->success."
			
			</p>";
			endif;
	
?>			
			<form role='forms' method='post' name='add_rule' id='add_rule' enctype = "multipart/form-data">
			<input type='hidden' name='selected_to_value' id='selected_to_value' value='<?php echo $_POST['rule_to_value']; ?>'>

			<div class='form-group'>
			
			<label for='rule_name'>Name</label> 
			
			<input type='text' class='form-control' name='rule_name' id='rule_name' placeholder='Enter rule name...' value="<?php echo stripcslashes($this->name);?>">
			
			
			</div>
			
		<div class='form-group'>
			
			<label for='rule_type'>Type</label>
			<br>
			<select  name='rule_type' id='rule_type'>
				<optgroup label="Page">
					<option value="page" <?php selected($_POST['role_type'],'page'); ?>>Page</option>
				</optgroup>
				<optgroup label="Post">
					<option value="post" <?php selected($_POST['role_type'],'post'); ?>>Post</option>
					<option value="post_category" <?php selected($_POST['role_type'],'post_category'); ?>>Post Category</option>
					<option value="post_type" <?php selected($_POST['role_type'],'post_type'); ?>>Post Type</option>
				</optgroup>
				<optgroup label="Other">
					<option value="widget" <?php selected($_POST['role_type'],'widget'); ?>>Widget</option>
					<option value="regx" <?php selected($_POST['role_type'],'regx'); ?>>URL REGX</option>
				</optgroup>
			</select>
			
			<select name='rule_match_me' id='rule_match_me'>
			
			<option value='1' <?php selected($_POST['match_me'],'1'); ?>>is equal to</option>
			
			<option value='2' <?php selected($_POST['match_me'],'2'); ?> >is not equal to</option>
		
			</select>
			
			<div id="rule_values_container" style="display:inline;">
			</div>
			</div>
			
					
			
			<input class='btn btn-default' type='submit' name='submit_add_rule' id='submit_add_rule' value='Save'>
			<input class='btn btn-default' type='button' name='cancel_add_rule' id='cancel_add_rule' value='Cancel' onclick="document.location='<?php echo wtm_set_action_url( array('action' => false, 'rule_id' => false , 'page' => 'wtm_manage', 'tab' => 'manage_rules')); ?>'">
			
			</form>
		 
		 </div>
		
			</div>
			
</div>
</div>
