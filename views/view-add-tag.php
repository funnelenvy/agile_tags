<div class='wrap'>
	<div class="container-fluid">	

	<h2>Add a Tag</h2>
					
			<div class='row'>
			
			<div class='col-sm-7'>

<?php			
			if( $this->errors != '' ) :
			echo "<p class='bg-danger'>
			
			".$this->display_errors()."
			
			</p>";
			endif;
			
			if( $this->success != '' ) :
			echo "<p class='bg-success'>
			
			".$this->success."
			
			</p>";
			endif;
	
?>			
			<form role='forms' method='post' name='add_tag' id='add_tag' enctype = "multipart/form-data">
			
			<div class='form-group'>
			
			<label for='tag_name'>Name</label> 
			
			<input type='text' class='form-control' name='tag_name' id='tag_name' placeholder='Enter tag name...' value="<?php echo stripcslashes($this->name);?>">
			
			
			</div>
			
			<div class='form-group'>
			
			<label for='tag_type'>Type</label>
			
			<select class='form-control' name='tag_type' id='tag_type'>
			
			<option value='1'>Custom HTML Tag</option>
			
			</select>
			
			</div>
			
			<div class='form-group'>
			
			<label>HTML</label>
		
			<?php wp_editor($this->content,'tag_content'); ?>
			
			</div>
			
			
			<input class='btn btn-default' type='submit' name='submit_add_tag' id='submit_add_tag' value='Save'>
			<input class='btn btn-default' type='button' name='cancel_add_tag' id='cancel_add_tag' value='Cancel' onclick="document.location='<?php echo wtm_set_action_url( array('action' => false, 'tag_id' => false, 'page' => 'wtm_manage')); ?>'">
			
			</form>
		 
		 </div>
		
			</div>
			
</div>
</div>
