<div class='wrap'>
	<div class="container-fluid">	

	<h2>Edit a Rule</h2>
					
			<div class='row'>
			
			<div class='col-sm-7'>

<?php			
			if( $this->errors != '' ) :
			echo "<p class='bg-danger'>
			
			".$this->display_errors()."
			
			</p>";
			endif;
			
			if( $this->success != '' ) :
			echo "<p class='bg-success'>
			
			".$this->success."
			
			</p>";
			endif;
	
?>			
			<form role='forms' method='post' name='add_rule' id='add_rule' enctype = "multipart/form-data">
			<input type='hidden' name='selected_to_value' id='selected_to_value' value='<?php echo $this->to_value; ?>'>
			<div class='form-group'>
			
			<label for='rule_name'>Name</label> 
			
			<input type='text' class='form-control' name='rule_name' id='rule_name' placeholder='Enter rule name...' value="<?php echo stripcslashes($this->name);?>">
			
			
			</div>
			
			<div class='form-group'>
			
			<label for='rule_type'>Type</label>
			<br>
			<select  name='rule_type' id='rule_type'>
				<optgroup label="Page">
					<option value="page" <?php selected($this->type,'page'); ?>>Page</option>
				</optgroup>
				<optgroup label="Post">
					<option value="post" <?php selected($this->type,'post'); ?>>Post</option>
					<option value="post_category" <?php selected($this->type,'post_category'); ?>>Post Category</option>
					<option value="post_type" <?php selected($this->type,'post_type'); ?>>Post Type</option>
				</optgroup>
				<optgroup label="Other">
					<option value="widget" <?php selected($this->type,'widget'); ?>>Widget</option>
					<option value="regx" <?php selected($this->type,'regx'); ?>>URL REGX</option>
				</optgroup>
			</select>
			
			<select name='rule_match_me' id='rule_match_me'>
			
			<option value='1' <?php selected($this->match_me,'1'); ?>>is equal to</option>
			
			<option value='2' <?php selected($this->match_me,'2'); ?> >is not equal to</option>
		
			</select>
			
			<div id="rule_values_container" style="display:inline;">
			</div>
			
			</div>
			
			
			<input class='btn btn-default' type='submit' name='submit_edit_rule' id='submit_edit_rule' value='Save'>
			<input class='btn btn-default' type='button' name='cancel_add_rule' id='cancel_add_rule' value='Cancel' onclick="document.location='<?php echo wtm_set_action_url( array('action' => false, 'rule_id' => false )); ?>'">
			
			</form>
		 
		 </div>
		
			<div class='col-sm-4'>
		
		<h4>Associated Tags</h4>
		
		<ul>

			<?php 
			if(isset($this->tags))
			{
				foreach($this->tags as $tag)
				{
					echo '<li><a href="'.wtm_set_action_url(array('tab'=> 'manage_tags', 'action' => 'edit', 'tag_id' => $tag->tag_id ,'rule_id' => false)).'">'.$tag->name.'</a></li>';
				}
			}
			else
			{
				echo '<li>No tags assoicated. </li>';
				
			}
			?>
			<?php
			?>
		</ul>

		
		</div>	
			
			</div>
			
</div>
</div>
