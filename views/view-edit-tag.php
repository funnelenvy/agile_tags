<div class='wrap'>
	<div class="container-fluid">	

	<h2>Edit a Tag</h2>
					
			<div class='row'>
			
			<div class='col-sm-7'>

<?php			
			if( $this->errors != '' ) :
			echo "<p class='bg-danger'>
			
			".$this->display_errors()."
			
			</p>";
			endif;
			
			if( $this->success != '' ) :
			echo "<p class='bg-success'>
			
			".$this->success."
			
			</p>";
			endif;
	
?>			
			<form role='forms' method='post' name='edit_tag' id='edit_tag' enctype = "multipart/form-data">
			
			<div class='form-group'>
			
			<label for='tag_name'>Name</label> 
			
			<input type='text' class='form-control' name='tag_name' id='tag_name' placeholder='Enter tag name...' value="<?php echo stripcslashes($this->name);?>">
			
			
			</div>
			
			<div class='form-group'>
			
			<label for='tag_type'>Type</label>
			
			<select class='form-control' name='tag_type' id='tag_type'>
			
			<option value='1'>Custom HTML Tag</option>
			
			</select>
			
			</div>
			
			<div class='form-group'>
			
			<label>HTML</label>
		
			<?php wp_editor($this->content,'tag_content'); ?>
			
			</div>
			
			
			<input class='btn btn-default' type='submit' name='submit_edit_tag' id='submit_edit_tag' value='Save'>
			<input class='btn btn-default' type='button' name='cancel_edit_tag' id='cancel_edit_tag' value='Cancel' onclick="document.location='<?php echo wtm_set_action_url( array('action' => false, 'tag_id' => false )); ?>'">
			
			</form>
		 
		 </div>
		
			<div class='col-sm-4'>
		
		<h4>Rules</h4>
		<form name='unassign_rules' id='unassign_rules' method='post'>
		<input type='hidden' name='rule_id' id='rule_id' value='0'>
		<input type='hidden' name='submit_unassign_rule' value='1'>
		<ul>

		<?php
		
		if(isset($this->rules))
		{
			foreach($this->rules as $rule)
			{
				?>
					<li> <?php echo $rule->name;?> <input type='button' onclick='unassign_rule(<?php echo $rule->rule_id; ?>)' class='btn btn-small unassign_rule' value='Delete'></li>
				<?php
				
				}	
		}
		else
		{
			echo '<li>No rules assigned yet.</li>';
		}	
		
		?>
	
		</ul>
		</form>
		
		<hr>
		
		<h4>Assign Rules</h4>
		<form name='assign_rules' id='assign_rules' method='post'>
		
		
		<ul>
			<li>
			<select name='rule' id='rule'> 
			<?php 
			
			$rule=new Rule();
			$all_rules=$rule->Get();

			if(isset($all_rules))
			{
				foreach($all_rules as $rule)
				{
				
				?>
						<option value="<?php echo $rule->getVal('rule_id'); ?>" ><?php echo $rule->getVal('name'); ?></option>
				<?php	
				
				}
				
			}
			?>
			</select> <input type='submit' name='submit_assign_rule' id='submit_assign_rule' class='btn btn-small' value='+ Add'></li>

		</ul>

		</form>
		
		
		</div>	
			
			</div>
			
</div>
</div>
