<div class="wrap">
<h2>Manage Rules <a class="add-new-h2" href="<?php echo wtm_set_action_url(array('action'=> 'add', 'rule_id' => false)); ?>">Add New Rule</a></h2>
<br>	
<div class="container-fluid">	
<div class='rows'><div class='col-sm-11'>
<div id="rules">
<!--
		        <div class="text-right"><a class="btn btn-default" href="<?php echo admin_url('admin.php?page=wtm_add_rule'); ?>">New Rule</a></div> 
-->
				<table class="table table-hover table-striped">
					<tr class="active">
						<th>Name</th>
						<th>Type</th>
						<th>Conditions</th>
						<th>Actions</th>
					</tr>
					<?php
					if(isset($all_rules))
					{
						foreach($all_rules as $rule)
						{
						
						?>	
					<tr>
						<td>
							<?php echo $rule->name; ?>
							<div class="row-actions">
							 <span class="edit"><a title="Edit <?php echo $rule->name; ?>" href="<?php echo wtm_set_action_url(array('action' => 'edit', 'rule_id' => $rule->rule_id)); ?>">Edit</a> | </span><span class="trash"><a href="<?php echo wtm_set_action_url( array('action' => 'delete', 'rule_id' => $rule->rule_id)); ?>" title="Delete this item" class="submitdelete">Delete</a> | </span></div>
						</td>
						<td><?php echo $rule->type; ?></td>
						<td><?php
						    
							if($rule->status == '1')
							{
								$status =  'Active';
								$btn_status = 'Pause';
								
							}
							else
							{
								$status = 'Pause';
								$btn_status = 'Active'; 	
							}
							echo $status;
								
							?></td>
						<td><a href="javascript:;" data-id="<?php echo $rule->rule_id; ?>" data-status="<?php echo $rule->status ? 1 : 0; ?>" class="rule-status"><?php
							echo $btn_status ;
								
							?></a></td> 
					</tr>
					<?php
				}
			}
			else
			{
					echo "<tr><td align='center' colspan='5'>Currently no rules added. <a href='".admin_url('admin.php?page=wtm_manage&tab=wtm_add_rule')."'> Click Here </a> to add a new rule.</td></tr>";
			}
			?>
				</table>
		    </div>
</div>
</div>
</div></div> 
