<div class="wrap">
<h2>Manage Tags <a class="add-new-h2" href="<?php echo wtm_set_action_url(array('action'=> 'add', 'tag_id' => false)); ?>">Add New Tag</a></h2>
<br>	
<div class="container-fluid">	
<?php 
if( $this->success != '' ) :
			echo "<p class='bg-success'>
			
			".$this->success."
			
			</p>";
			endif;
?>
<div class='row'>		
		<div class='col-sm-11'>
<!--
<div class="text-right"><a class="btn btn-default" href="<?php echo admin_url('admin.php?page=wtm_add_tag'); ?>">New Tag</a></div> 
-->

			<table class="table table-hover table-striped">
					<tr class="active">
						<th>Name</th>
						<th>Type</th>
						<th>Rules</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
					<?php if(isset($all_tags)) {
						
						foreach($all_tags as $tag)
						{
							?>
					<tr>
						<td>
							<?php echo $tag->name; ?>
							<div class="row-actions">
							 <span class="edit"><a title="Edit <?php echo $tag->name; ?>" href="<?php echo wtm_set_action_url(array('action' => 'edit', 'tag_id' => $tag->tag_id)); ?>">Edit</a> | </span><span class="trash"><a href="<?php echo wtm_set_action_url(array('action' => 'delete', 'tag_id' => $tag->tag_id)); ?>" title="Delete this item" class="submitdelete">Delete</a> | </span></div>
						</td>
						<td><?php echo $this->tag_types[$tag->type]; ?></td>
						<td><a href="#"><?php
						if(count($tag->rules)>0)
						{
						$all = array();	
						foreach($tag->rules as $rule)
						{
							$all[]= "<a href='". wtm_set_action_url(array('tab'=> 'manage_rules', 'action' => 'edit', 'rule_id' => $rule->rule_id))."'>".$rule->name.'</a>';
						}	
						
						echo implode(' | ',$all);
					}
					else
					echo '-';		
						?></a></td>
						<td>
							<?php
							if($tag->status==1)
							{
								$status =  'Active';
								$btn_status = 'Pause';
								
							}
							else
							{
								$status = 'Pause';
								$btn_status = 'Active'; 	
							}
							
							echo $status;
								
							?>
						</td>
						<td><a href="javascript:;" data-id="<?php echo $tag->tag_id; ?>" data-status="<?php echo $tag->status ? 1 : 0; ?>" class="tag-status"><?php echo $btn_status ; ?></a></td> 
					</tr>
				<?php
			}
			
		}else
			{
					echo "<tr><td align='center' colspan='5'>Currently no tags added. <a href='".admin_url('admin.php?page=wtm_manage&tab=wtm_add_tag')."'> Click Here </a> to add a new tag.</td></tr>";
			}
		?>
	</table>	
</div>
</div>
</div>
</div>
